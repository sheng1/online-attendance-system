package attendance.web;

import attendance.client.ModifyStaff;
import attendance.client.Staff;
import attendance.db.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.*;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


/**
 * 系统主页控制类
 *
 * @author lx
 * @version v1.0
 */
@Controller // 控制定义
@RequestMapping("/staff") // 相应web路径
public class StaffController {

    @Autowired // 自动注入资源
    private StaffRepository staffRepository;

    /**
     * 登陆后首页
     *
     * @return 登陆后首页
     */
    @RequestMapping(method = GET)
    public String homepage() {
        return "client/homePage";
    }

    /**
     * 处理首页打卡操作
     *
     * @param session session
     * @return 跳转页面
     */
    @RequestMapping(method = POST) // 处理首页打卡操作
    public String doAttendance(HttpSession session) {
        Staff staff = (Staff) session.getAttribute("staff");
        String id = staff.getUsername();
        boolean flag = staffRepository.addAttendance(id);
        session.setAttribute("flag", flag);
        return "client/homePage";
    }

    /**
     * 员工注册
     *
     * @return 注册页面
     */
    @RequestMapping(value = "/register", method = GET)
    public String getRegister(Model model) {
        model.addAttribute("staff", new Staff());
        return "client/register";
    }

    /**
     * 注册提交
     *
     * @return 跳转页面
     */
    @RequestMapping(value = "/register", method = POST)
    public String processRegister(@Valid @ModelAttribute("staff") Staff staffNew, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "client/register";

        }
        staffRepository.addStaff(staffNew);
        session.setAttribute("staff", staffNew);
        return "redirect:/staff";
    }

    /**
     * 员工登录请求
     *
     * @return 跳转页面
     */
    @RequestMapping(value = "/login", method = GET)
    public String enterLogin(HttpServletRequest request, Model model) {
        Cookie[] cookies = request.getCookies();
        String uname = null;
        // 在cookie不为空的情况下把账号密码赋值给uname和upwd
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().compareTo("staffName") == 0) {
                    uname = cookie.getValue();
                    System.out.println(uname);
                }
            }
        }
        model.addAttribute("name", uname);
        return "client/loginForm";
    }

    /**
     * 员工登录提交
     *
     * @param userName 用户名
     * @param password 密码
     * @param session  session
     * @return 跳转页面
     */
    @RequestMapping(value = "/login", method = POST)
    public String processLogin(@RequestParam(value = "username", defaultValue = "") String userName,
                               @RequestParam(value = "password", defaultValue = "") String password, HttpSession session, HttpServletResponse res) {

        Cookie name = new Cookie("staffName", userName);
        name.setMaxAge(60 * 24 * 7);
        res.addCookie(name);
        Staff staff = staffRepository.findByUserName(userName, password);
        if (staff != null) {
            session.setAttribute("staff", staff);
            return "redirect:/staff";
        } else {
            return "client/loginError";
        }

    }

    /**
     * 注销
     *
     * @param session session
     * @return 跳转页面
     */
    @RequestMapping(value = "/logout", method = GET)
    public String logout(HttpSession session, HttpServletResponse res) {
        Cookie newCookie = new Cookie("staffName", null); //假如要删除username的Cookie
        newCookie.setMaxAge(0); //立即删除型
        newCookie.setPath("/"); //项目所有目录均有效，这句很关键，否则不敢保证删除
        res.addCookie(newCookie);

        session.removeAttribute("staff");
        session.invalidate();
        return "redirect:/";
    }


    /**
     * 更新员工信息
     *
     * @param model   模型
     * @param session session
     * @return 跳转页面
     */
    @RequestMapping(value = "/modifyinformation", method = GET)
    public String modify(Model model, HttpSession session) {
        Staff staff = (Staff) session.getAttribute("staff");
        model.addAttribute("modifystaff", new ModifyStaff(staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getAddress()));
        return "client/modify";
    }


    /**
     * 更新员工信息，同时更新seesion中的staff
     *
     * @param staffinfo 员工信息
     * @param errors    错误
     * @param session   session
     * @return 跳转页面
     */
    @RequestMapping(value = "/modifyinformation", method = POST)
    public String processModify(@Valid ModifyStaff staffinfo, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "client/modify";
        }
        Staff staff = (Staff) session.getAttribute("staff");
        staffRepository.modify(staffinfo, staff.getId());
        Staff staff1 = staffRepository.findByUserName(staff.getId());
        session.setAttribute("staff", staff1);
        return "redirect:/staff";
    }

    /**
     * 查看出勤列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param model    模型
     * @param session  session
     * @return 跳转页面
     */
    @RequestMapping(value = "/attendancerecord", method = GET)
    public String attendance(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                             @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model, HttpSession session) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        Staff staff = (Staff) session.getAttribute("staff");
        model.addAttribute("attendancepagSupport", staffRepository.attendancePage(pageNo, pageSize, staff.getUsername()));
        return "client/attendance";
    }

    /**
     * 请假
     *
     * @return 跳转页面
     */
    @RequestMapping(value = "/leave", method = GET)
    public String leaveForm(Model model) {
        model.addAttribute("leavereason", staffRepository.getLeaveReason());
        return "client/leave";
    }

    /**
     * 提交请假，并且检验请假是否满足规范（这你改改，reason改成复选框，管理员维护的那个）
     *
     * @param leaveDate 请假日期
     * @param leaveTime 请假时长
     * @param reason    请假原因
     * @param model     模型
     * @param session   session
     * @return 跳转页面
     */
    @RequestMapping(value = "/leave", method = POST)
    public String delete0(@RequestParam("leavedate") String leaveDate, @RequestParam("leavetime") int leaveTime, @RequestParam("reason") String reason, Model model, HttpSession session)
            throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(leaveDate);
        if (date.before(new Date())) {
            JOptionPane.showMessageDialog(null, "不能请过去的假！", "操作失败", JOptionPane.ERROR_MESSAGE);
            return "redirect:/staff/leave";
        } else if (leaveTime < 1) {
            JOptionPane.showMessageDialog(null, "请假天数必须大于等于1！", "操作失败", JOptionPane.ERROR_MESSAGE);
            return "redirect:/staff/leave";
        }
        Staff staff = (Staff) session.getAttribute("staff");
        staffRepository.addLeave(date, leaveTime, reason, staff.getUsername());
        return "redirect:/staff";
    }

    /**
     * 请假分页
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param model    模型
     * @param session  session
     * @return 跳转页面
     */
    @RequestMapping(value = "/leaveList", method = GET)
    public String leaveList(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                            @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                            Model model, HttpSession session) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        Staff staff = (Staff) session.getAttribute("staff");
        model.addAttribute("leavepagSupport", staffRepository.leavePage(pageNo, pageSize, staff.getUsername()));
        return "/client/leaveList";
    }

}
