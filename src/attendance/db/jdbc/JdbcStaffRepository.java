package attendance.db.jdbc;

import attendance.client.*;
import attendance.db.StaffRepository;
import attendance.web.PaginationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 员工资源库实现类
 *
 * @author wang sen
 * @version 1.0
 */
@Repository
public class JdbcStaffRepository implements StaffRepository {
    private final JdbcTemplate jdbc;

    @Autowired
    public JdbcStaffRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    /**
     * Staff类RowMapper
     */
    protected static class StaffRowMapper implements RowMapper<Staff> {
        @Override
        public Staff mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Staff(rs.getLong("id"), rs.getString("username"), null, rs.getString("Name"),
                    rs.getString("email"), rs.getString("phoneNo"), rs.getInt("department_id"), rs.getString("address"));
        }
    }

    /**
     * Attendance类RowMapper
     */
    protected static class AttendanceRowMapper implements RowMapper<Attendance> {
        @Override
        public Attendance mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Attendance(rs.getLong("id"), rs.getTimestamp("attendancetime"), rs.getString("staff_username"), rs.getInt("state"));
        }
    }

    /**
     * Leave类RowMapper
     */
    protected static class LeaveRowMapper implements RowMapper<Leave> {
        @Override
        public Leave mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Leave(rs.getLong("id"), rs.getString("staff_username"), rs.getDate("leavetime"), rs.getInt("Duration"), rs.getInt("state"), rs.getString("manager_username"), rs.getString("reason"));
        }
    }

    /**
     * LeaveReason类RowMapper
     */
    protected static class LeaveReasonRowMapper implements RowMapper<LeaveReason> {
        @Override
        public LeaveReason mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new LeaveReason(rs.getLong("id"), rs.getString("reason"));
        }
    }

    /**
     * 添加员工
     *
     * @param staff 员工
     */
    @Override
    public void addStaff(Staff staff) {
        jdbc.update("insert into staff (username, password, name, email, phoneNo, department_id, address) values (?,?,?,?,?,?,?)", staff.getUsername(), staff.getPassword(), staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getDepartment_id(), staff.getAddress());
    }

    /**
     * 通过用户名和密码查找员工
     *
     * @param userName 用户名
     * @param password 密码
     * @return 员工
     */
    @Override
    public Staff findByUserName(String userName, String password) {
        Staff staff = null;
        try {
            staff = jdbc.queryForObject("select * from staff where username=? and password=?", new StaffRowMapper(),
                    userName, password);
        } catch (DataAccessException ignored) { }
        return staff;
    }

    /**
     * 添加考勤
     *
     * @param username 用户名
     * @return 布尔值
     */
    @Override
    public boolean addAttendance(String username) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String str = sdf.format(date);
        int year, month, day;
        year = Integer.parseInt(str.split("-")[0]);
        month = Integer.parseInt(str.split("-")[1]);
        day = Integer.parseInt(str.split("-")[2]);
        Attendance attendance = null;
        try {
            attendance = jdbc.queryForObject("select * from attendance where staff_username=? and year=? and month=? and day=? and state=1",
                    new AttendanceRowMapper(), username, year, month, day);
        } catch (DataAccessException ignored) { }
        if (attendance == null) {
            jdbc.update("insert into attendance (Attendancetime, staff_username,year, month, day,state) values (?, ?, ?, ?, ?, ?)", date, username, year, month, day, 1);
            return true;
        } else {
            return false;
        }

    }

    /**
     * 更新员工信息
     *
     * @param staffInfo 员工信息
     * @param id        ID
     */
    @Override
    public void modify(ModifyStaff staffInfo, Long id) {
        jdbc.update("update staff set name=? where id=?", staffInfo.getName(), id);
        jdbc.update("update staff set email=? where id=?", staffInfo.getEmail(), id);
        jdbc.update("update staff set phoneNo=? where id=?", staffInfo.getPhoneNo(), id);
        jdbc.update("update staff set address=? where id=?", staffInfo.getAddress(), id);
    }

    /**
     * 通过员工ID查找员工
     *
     * @param id 员工ID
     * @return 员工
     */
    @Override
    public Staff findByUserName(Long id) {
        Staff staff = null;
        try {
            staff = jdbc.queryForObject("select * from staff where id=?", new StaffRowMapper(),
                    id);
        } catch (DataAccessException ignored) { }
        return staff;
    }


    /**
     * 用户出勤分页查看
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param username 用户名
     * @return 用户出勤分页
     */
    @Override
    public PaginationSupport<Attendance> attendancePage(int pageNo, int pageSize, String username) {
        int totalCount = jdbc.queryForInt("select count(id) from attendance where staff_username=?", username);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1)
            return new PaginationSupport<>(new ArrayList<>(0), 0);

        List<Attendance> items = jdbc.query("select * from attendance where staff_username=? order by id desc limit ? offset  ?", new AttendanceRowMapper(), username, pageSize, startIndex);
        System.out.println(items.get(0).getAttendancetime());
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 添加请假到数据库中
     *
     * @param leavedate 请假日期
     * @param leavetime 请假时长
     * @param reason    请假原因
     * @param username  用户名
     */
    @Override
    public void addLeave(Date leavedate, int leavetime, String reason, String username) {
        jdbc.update("insert into `leave`(staff_username,leavetime,Duration,manager_username,state,reason)values (?,?,?,?,0,?)", username, leavedate, leavetime, null, reason);
    }

    /**
     * 某个用户的请假分页查看
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param username 用户名
     * @return 请假分页
     */
    @Override
    public PaginationSupport<Leave> leavePage(int pageNo, int pageSize, String username) {
        int totalCount = jdbc.queryForInt("select count(id) from `leave` where staff_username=? ", username);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1)
            return new PaginationSupport<>(new ArrayList<>(0), 0);

        List<Leave> items = jdbc.query("select * from `leave` where staff_username=? order by id desc limit ? offset  ?", new LeaveRowMapper(), username, pageSize, startIndex);
        return new PaginationSupport<Leave>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 从数据库中查看请假事由
     *
     * @return 请假事由
     */
    @Override
    public List<LeaveReason> getLeaveReason() {
        return jdbc.query("select * from leavereason ", new LeaveReasonRowMapper());
    }

}
