package attendance.system;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 管理员类
 */
public class Manager {
    /**
     * id
     */
    private Long id;
    /**
     * 用户名
     */
    @NotNull
    @Size(min = 1, max = 20)
    private String username;
    /**
     * 密码
     */
    @NotNull
    @Size(min = 5, max = 25)
    private String password;
    /**
     * 全名
     */
    @NotNull
    @Size(min = 1, max = 40)
    private String name;
    /**
     * 邮箱
     */
    @NotNull
    @Email
    private String email;

    /**
     * 构造函数
     *
     * @param id       管理员ID
     * @param username 用户名
     * @param password 密码
     * @param name     姓名
     * @param email    邮件
     */
    public Manager(Long id, String username, String password, String name, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
    }

    /**
     * 获取ID
     *
     * @return id
     */
    public Long getId() {
        return id;
    }


    /**
     * 设置id
     *
     * @param id 管理员id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return 用户名
     */

    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取姓名
     *
     * @return 姓名
     */
    public String getName() {
        return name;
    }


    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取邮件
     *
     * @return 邮件
     */
    public String getEmail() {
        return email;
    }


    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
