package attendance.client;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * 考勤类
 *
 * @author wang sen
 * @version 1.0
 */
public class Attendance {
    /**
     * 考勤ID
     */
    private Long id;
    /**
     * 考勤时间
     */
    @NotNull
    private Timestamp Attendancetime;
    /**
     * 员工姓名
     */
    @NotNull
    @Size(min = 5, max = 16)
    private String staff_username;
    /**
     * Attendancetime拆出来的
     */
    private int year;
    @NotNull
    private int month;
    @NotNull
    private int day;
    /**
     * 2是请假，1是已打卡，0是缺勤
     */
    @NotNull
    private int state;

    /**
     * 构造函数A
     *
     * @param staff_username 员工姓名
     * @param state          请假状态
     */
    public Attendance(String staff_username, int state) {
        this.staff_username = staff_username;
        this.state = state;
    }

    /**
     * 构造函数B
     *
     * @param id             考勤ID
     * @param attendancetime 考勤时间
     * @param staff_username 考勤员工姓名
     * @param state          考勤状态
     */
    public Attendance(Long id, Timestamp attendancetime, String staff_username, int state) {
        this.id = id;
        this.Attendancetime = attendancetime;
        this.staff_username = staff_username;
        this.state = state;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("attendancetime" + attendancetime);
        String str = sdf.format(attendancetime);
        this.year = Integer.parseInt(str.split("-")[0]);
        this.month = Integer.parseInt(str.split("-")[1]);
        this.day = Integer.parseInt(str.split("-")[2]);
    }

    /**
     * 获取考勤ID
     *
     * @return id 考勤的ID
     */

    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id 考勤ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取考勤时间
     *
     * @return Attendancetime 考勤时间
     */
    public Timestamp getAttendancetime() {
        return Attendancetime;
    }

    /**
     * 设置考勤时间
     *
     * @param attendancetime 考勤时间
     */
    public void setAttendancetime(Timestamp attendancetime) {
        Attendancetime = attendancetime;
    }

    /**
     * 获取员工姓名
     *
     * @return staff_username 员工姓名
     */
    public String getStaff_username() {
        return staff_username;
    }

    /**
     * 设置员工姓名
     *
     * @param staff_username 员工姓名
     */
    public void setStaff_username(String staff_username) {
        this.staff_username = staff_username;
    }

    /**
     * 获取考勤时间中的年份
     *
     * @return year 考勤时间中的年份year
     */
    public int getYear() {
        return year;
    }

    /**
     * 设置年份
     *
     * @param year 考勤时间中的年份
     */
    public void setYear(int year) {
        this.year = year;
    }


    /**
     * 获取月份
     *
     * @return month 月份
     */
    public int getMonth() {
        return month;
    }

    /**
     * 设置月份month
     *
     * @param month 考勤时间中的月份
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * 获取日期day
     *
     * @return day 日期
     */
    public int getDay() {
        return day;
    }

    /**
     * 设置日期day
     *
     * @param day 日期
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * 获取考勤状态
     *
     * @return state 考勤状态
     */
    public int getState() {
        return state;
    }

    /**
     * 设置考勤状态
     *
     * @param state 考勤状态
     */

    public void setState(int state) {
        this.state = state;
    }
}
	
	
