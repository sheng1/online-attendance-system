package attendance.client;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 员工类
 *
 * @author wang sen
 * @version 1.0
 */
public class Staff {
    /**
     * 员工id
     */
    private Long id;
    /**
     * 员工用户名
     */
    @NotNull
    @Size(min = 1, max = 16)
    private String username;
    /**
     * 员工的密码
     */
    @NotNull
    @Size(min = 5, max = 16)
    private String password;
    /**
     * 员工的姓名
     */
    @NotNull
    @Size(min = 1, max = 16)
    private String name;
    /**
     * 员工的邮箱
     */
    @Email
    @NotNull
    private String email;
    /**
     * 员工的手机号
     */
    @NotNull
    @Pattern(regexp = "^((0\\d{2,3}-\\d{7,8})|(1[34578]\\d{9}))$", message = "Wrong phone number")
    private String phoneNo;
    /**
     * 员工的部门ID
     */
    @NotNull
    private long department_id;
    /**
     * 员工的家庭住址
     */
    @NotNull
    @Size(min = 1, max = 16)
    private String address;

    /**
     * 构造函数
     */
    public Staff() {
    }

    /**
     * 构造函数
     *
     * @param id            员工ID
     * @param username      用户名
     * @param password      密码
     * @param name          姓名
     * @param email         邮箱
     * @param phoneNo       手机号
     * @param department_id 部门ID
     * @param address       家庭住址
     */
    public Staff(Long id, String username, String password, String name, String email, String phoneNo,
                 long department_id, String address) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
        this.department_id = department_id;
        this.address = address;
    }

    /**
     * 获取员工ID
     *
     * @return id 员工ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id 员工ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return username 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取员工密码
     *
     * @return password 密码
     */

    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取员工姓名
     *
     * @return name 员工姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置员工姓名
     *
     * @param name 员工姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取邮箱
     *
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取手机号
     *
     * @return phoneNo 手机号
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * 设置手机号
     *
     * @param phoneNo 手机号
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * 获取部门ID
     *
     * @return department_id 部门ID
     */
    public long getDepartment_id() {
        return department_id;
    }

    /**
     * 设置部门id
     *
     * @param department_id 部门Id
     */
    public void setDepartment_id(long department_id) {
        this.department_id = department_id;
    }

    /**
     * 获取家庭住址
     *
     * @return address 家庭住址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置家庭住址
     *
     * @param address 家庭住址
     */
    public void setAddress(String address) {
        this.address = address;
    }
}
