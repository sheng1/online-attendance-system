package attendance.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * 显示当前时间的Tag
 *
 * @author 2018303010
 * @version 1.0
 */
public class DateTag extends SimpleTagSupport {

    /**
     * 实现doTag方法
     *
     * @throws JspException Jsp页面异常
     */
    @Override
    public void doTag() throws JspException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            JspWriter out = getJspContext().getOut();
            out.write(df.format(new java.util.Date()));
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
    }
}