<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
    <title>管理员登录</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<div id="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>
<div>
    <div class="login">
        <h1>管理员登陆</h1>
        <form method="POST">
            <label>
                <a>用户名:</a><input type="text" placeholder="UserName" name="username" class="txtb" />
            </label>
            <label>
                <a>密码:</a><input type="password" placeholder="Password" name="password" class="txtb"/>
            </label>
            <br/>
            <label>
                <input type="submit" value="登录" class="login-btn"/>
            </label>
        </form>
    </div>
</div>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
