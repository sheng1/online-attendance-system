<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>请假信息列表</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<div>
    <a href="<c:url value="/manager/leaves"/>"><input type="button" value="返回"></a>&nbsp;
    <a href="<c:url value="/manager/addReason"/>"><input type="button" value="添加"></a>&nbsp;
    <br/><br/>
</div>
<div class="departmentList">
    <form method="post">
        <table>
            <tr>
                <th>ID</th>
                <th>信息</th>
                <th>删除</th>
            </tr>
            <c:forEach items="${leave_reason.items}" var="department">
                <tr id="staff_<c:out value="${department.id}"/>">
                    <td><c:out value="${department.id}"/></td>
                    <td><c:out value="${department.reason}"/></td>
                    <td><label>
                        <input type="checkbox" name="delete" value="${department.id}">
                    </label></td>
                    <td>
                        <a href="<c:url value="/manager/leaveReason/${department.id}"/>">
                            <input type="button" value="修改"></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <input type="submit" value="执行"/>
    </form>
</div>
<br/><br/>
<div class="page">
    每页${leave_reason.pageSize}个请假信息，
    第${leave_reason.currentPageNo}/${leave_reason.totalPageCount}页，共${leave_reason.totalCount}个请假信息
    <c:if test="${leave_reason.previousPage}">
        <c:choose>
            <c:when test="${leave_reason.pageSize eq leave_reason.PAGESIZE}">
                <a href="<c:url value="/manager/leaveReasons?pageNo=${leave_reason.currentPageNo - 1}" />">上一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/leaveReasons?pageNo=${leave_reason.currentPageNo - 1}&pageSize=${leave_reason.pageSize}" />">上一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:if test="${leave_reason.nextPage}">
        <c:choose>
            <c:when test="${leave_reason.pageSize eq leave_reason.PAGESIZE}">
                <a href="<c:url value="/manager/leaveReasons?pageNo=${leave_reason.currentPageNo + 1}" />">下一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/leaveReasons?pageNo=${leave_reason.currentPageNo + 1}&pageSize=${leave_reason.pageSize}" />">下一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>
</body>
</html>