<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>部门列表</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<div>
    <a href="<c:url value="/manager"/>"><input type="button" value="返回"></a>&nbsp;
    <a href="<c:url value="/manager/addDepartment"/>"><input type="button" value="添加部门" style="width: 80px"></a>&nbsp;
    <br/><br/>
</div>
<div class="departmentList">
    <form method="post">
        <table>
            <tr>
                <th>部门ID</th>
                <th>部门名</th>
                <th>父部门ID</th>
                <th>删除</th>
            </tr>
            <c:forEach items="${department_list.items}" var="department">
                <tr id="staff_<c:out value="${department.id}"/>">
                    <td><c:out value="${department.id}"/></td>
                    <td><c:out value="${department.name}"/></td>
                    <td><c:out value="${department.parentID}"/></td>
                    <td><label>
                        <input type="checkbox" name="delete" value="${department.id}">
                    </label></td>
                    <td>
                        <a href="<c:url value="/manager/department/${department.id}"/>">
                            <input type="button" value="修改"></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <input type="submit" value="执行"/>
    </form>
</div>
<br/><br/>
<div>
    每页${department_list.pageSize}个部门，
    第${department_list.currentPageNo}/${department_list.totalPageCount}页，共${department_list.totalCount}页
    <c:if test="${department_list.previousPage}">
        <c:choose>
            <c:when test="${department_list.pageSize eq department_list.PAGESIZE}">
                <a href="<c:url value="/manager/departments?pageNo=${department_list.currentPageNo - 1}" />">上一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/departments?pageNo=${department_list.currentPageNo - 1}&pageSize=${department_list.pageSize}" />">上一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:if test="${department_list.nextPage}">
        <c:choose>
            <c:when test="${department_list.pageSize eq department_list.PAGESIZE}">
                <a href="<c:url value="/manager/departments?pageNo=${department_list.currentPageNo + 1}" />">下一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/departments?pageNo=${department_list.currentPageNo + 1}&pageSize=${department_list.pageSize}" />">下一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>
</body>
</html>