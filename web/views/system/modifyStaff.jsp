<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>修改员工信息</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css" />">
</head>
<body>
<h1>修改员工信息</h1>
<sf:form method="POST" commandName="staff"> <sf:errors cssClass="error"/><br/>
    <p>　　姓名：<sf:input path="name"/> <sf:errors path="name" cssClass="error"/></p>
    <p>　　邮箱：<sf:input path="email"/> <sf:errors path="email" cssClass="error"/></p>
    <p>电话号码：<sf:input path="phoneNo"/> <sf:errors path="phoneNo" cssClass="error"/></p>
    <p>　　地址：<sf:input path="address"/> <sf:errors path="address" cssClass="error"/></p><br/>
    <input type="submit" value="修改"/>
</sf:form>
<a href="<c:url value="/manager/staffs"/>"><input type="button" value="返回"></a><br/>
</body>
</html>