<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>员工列表</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<div>
    <a href="<c:url value="/manager"/>"><input type="button" value="返回"></a>&nbsp;
    <a href="<c:url value="/manager/addStaff"/>"><input type="button" value="添加员工" style="width: 80px"></a>&nbsp;
    <br/><br/>
</div>
<div class="staffList">
    <form method="post">
        <table>
            <tr>
                <th>ID</th>
                <th>用户名</th>
                <th>姓名</th>
                <th>邮箱</th>
                <th>电话号码</th>
                <th>部门ID</th>
                <th>地址</th>
                <th>删除</th>
            </tr>
            <c:forEach items="${staff_list.items}" var="department">
                <tr id="staff_<c:out value="${department.id}"/>">
                    <td><c:out value="${department.id}"/></td>
                    <td><c:out value="${department.username}"/></td>
                    <td><c:out value="${department.name}"/></td>
                    <td><c:out value="${department.email}"/></td>
                    <td><c:out value="${department.phoneNo}"/></td>
                    <td><c:out value="${department.department_id}"/></td>
                    <td><c:out value="${department.address}"/></td>
                    <td><label>
                        <input type="checkbox" name="delete" value="${department.id}">
                    </label></td>
                    <td>
                        <a href="<c:url value="/manager/staff/${department.id}"/>"><input type="button" value="修改"></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <input type="submit" value="执行"/>
    </form>
</div>
<br/><br/>
<div class="page">
    每页${staff_list.pageSize}个员工，
    第${staff_list.currentPageNo}/${staff_list.totalPageCount}页，共${staff_list.totalCount}个员工
    <c:if test="${staff_list.previousPage}">
        <c:choose>
            <c:when test="${staff_list.pageSize eq staff_list.PAGESIZE}">
                <a href="<c:url value="/manager/staffs?pageNo=${staff_list.currentPageNo - 1}" />">上一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/staffs?pageNo=${staff_list.currentPageNo - 1}&pageSize=${staff_list.pageSize}" />">上一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:if test="${staff_list.nextPage}">
        <c:choose>
            <c:when test="${staff_list.pageSize eq staff_list.PAGESIZE}">
                <a href="<c:url value="/manager/staffs?pageNo=${staff_list.currentPageNo + 1}" />">下一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/staffs?pageNo=${staff_list.currentPageNo + 1}&pageSize=${staff_list.pageSize}" />">下一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>
</body>
</html>