<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>添加部门</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<h1>添加部门</h1>
<sf:form method="POST" commandName="department_new"> <sf:errors cssClass="error"/><br/>
    <p>部门ＩＤ：<sf:input path="id"/> <sf:errors path="id" cssClass="error"/></p><br/><br/>
    <p>　部门名：<sf:input path="name"/> <sf:errors path="name" cssClass="error"/></p><br/><br/>
    <p>　父ＩＤ：<sf:input path="parentID"/> <sf:errors path="parentID" cssClass="error"/></p><br/><br/>
    <input type="submit" value="添加"/>
</sf:form>
<a href="<c:url value="/manager/departments"/>"><input type="button" value="返回"></a><br/>
</body>
</html>
