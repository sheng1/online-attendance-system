<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="MyFirstTag" prefix="mytag" %>
<html>
<head>
    <title>出错了</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<div id="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>
<div>
    <p><mytag:error></mytag:error></p>
</div>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
