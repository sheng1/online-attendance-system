<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="MyFirstTag" prefix="mytag" %>
<html>
<head>
    <title>请假</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />">
</head>
<body>

<form class="topdiv" method="POST">
    <label>
        请假时间
        <input type="date" name="leavedate" required="required">
    </label><br>
    <label>
        请假时长
        <input type="number" name="leavetime" required="required">
    </label><br>
    <label>
        事由
        <mytag:reasons/>
    </label>
    <input type="submit" value="请假"/>
</form>
<a style="color: black" href="<c:url value="/staff"/>"><input type="button" value="返回"></a><br/>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
