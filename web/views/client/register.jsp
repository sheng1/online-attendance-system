<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>员工注册</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />" >
</head>
<div id="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>

<div>
    <div>
        <h2 style="color: white">员工注册</h2>
        <sf:form method="POST" commandName="staff"> <sf:errors cssClass="error"/><br/>
            　<a>用户名：</a><sf:input path="username"/> <sf:errors path="username" cssClass="error"/><br/><br/>
            　　<a>密码：</a><sf:password path="password"/> <sf:errors path="password" cssClass="error"/><br/><br/>
            　　<a>姓名：</a><sf:input path="name"/> <sf:errors path="name" cssClass="error"/><br/><br/>
            　　<a>邮箱：</a><sf:input path="email"/> <sf:errors path="email" cssClass="error"/> <br/><br/>
            <a>电话号码：</a><sf:input path="phoneNo"/> <sf:errors path="phoneNo" cssClass="error"/><br/><br/>
            <a>部门ＩＤ：</a><sf:input path="department_id"/> <sf:errors path="department_id" cssClass="error"/><br/><br/>
            　　<a>地址：</a><sf:input path="address"/> <sf:errors path="address" cssClass="error"/><br/><br/>
            <input type="submit" value="注册"/>
        </sf:form>
        <a style="color: black" href="<c:url value="/"/>"><input type="button" value="返回"></a><br/>
    </div>
</div>
</body>
<div id="botdiv">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>