<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>修改信息</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="../resources/style.css" />">
</head>

<div class="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>
<h1>修改</h1>
<div class="login">
    <sf:form method="POST" commandName="modifystaff">
        <p>Name:</p><sf:input path="name"/><sf:errors path="name" cssClass="error"/> <br/><br/>
        <p>email:</p><sf:input path="email"/><sf:errors path="email" cssClass="error"/><br/><br/>
        <p>phoneNo:</p><sf:input path="phoneNo"/><sf:errors path="phoneNo" cssClass="error"/> <br/><br/>
        <p>address:</p><sf:input path="address"/><sf:errors path="address" cssClass="error"/> <br/><br/>
        <input type="submit" value="修改"/><br/>
        <a style="color: black" href="<c:url value="/staff"/>"><input type="button" value="返回"></a><br/>
    </sf:form>
</div>
</body>

<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>