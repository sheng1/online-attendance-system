<%--
  Created by IntelliJ IDEA.
  User: lx
  Date: 2020/12/11
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>员工登录</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />">
</head>
<div id="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>

<div>
    <div class="login">

        <h1>员工登录</h1>
        <form method="POST">
            <a>用户名:</a><input type="text" placeholder="UserName" name="username" class="txtb" value="${name}"/>
            <a>密码:</a><input type="password" placeholder="Password" name="password" class="txtb"/>
            <input type="submit" value="登录" class="login-btn"/>
        </form>
    </div>
</div>
</body>

<div id="botdiv">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
