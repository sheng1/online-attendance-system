<%@ page contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>签到信息</title>

    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />">
</head>
<body>
<div class="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<c:forEach items="${attendancepagSupport.items}" var="department">

    <li id="spitter_<c:out value="${department.id}"/>">
        <c:out value="${department.id}"> </c:out>
        <c:out value="${department.staff_username}"> </c:out>
        <fmt:formatDate value="${department.attendancetime}" pattern="yyyy-MM-dd HH:mm:ss"/>
        <c:choose>
            <c:when test="${department.state==1}">已签到</c:when>
        </c:choose>

    </li>
</c:forEach>
每页${attendancepagSupport.pageSize}个签到信息， 第${attendancepagSupport.currentPageNo}/${attendancepagSupport.totalPageCount}页,共${attendancepagSupport.totalCount}个签到
<c:if test="${attendancepagSupport.previousPage}">
    <a href="<c:url value="/staff/attendancerecord?pageNo=${attendancepagSupport.currentPageNo-1}" />">上一页</a>
</c:if>
<c:if test="${attendancepagSupport.nextPage}">
    <a href="<c:url value="/staff/attendancerecord?pageNo=${attendancepagSupport.currentPageNo+1}" />">下一页</a>
</c:if>
<br/>
<a style="color: black" href="<c:url value="/staff"/>"><input type="button" value="返回"></a>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>