<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>请假列表</title>
</head>
<div class="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>

<c:forEach items="${leavepagSupport.items}" var="department">

    <li id="spitter_<c:out value="${department.id}"/>">
        请假日期<fmt:formatDate value="${department.leavetime}" pattern="yyyy-MM-dd"/>
        请假时长<c:out value="${department.duration}"> </c:out>天
        <c:choose>
            <c:when test="${department.state==0}">尚未通过</c:when>
            <c:when test="${department.state==1}">审核通过</c:when>
        </c:choose>

    </li>
</c:forEach>
<div class="page">
    每页${leavepagSupport.pageSize}个请假信息，
    第${leavepagSupport.currentPageNo}/${leavepagSupport.totalPageCount}页,共${leavepagSupport.totalCount}个请假
    <c:if test="${leavepagSupport.previousPage}">
        <a href="<c:url value="/staff/leaveList?pageNo=${leavepagSupport.currentPageNo-1}" />">上一页</a>
    </c:if>
    <c:if test="${leavepagSupport.nextPage}">
        <a href="<c:url value="/staff/leaveList?pageNo=${leavepagSupport.currentPageNo+1}" />">下一页</a>
    </c:if>

    <a style="color: black" href="<c:url value="/staff"/>"><input type="button" value="返回"></a><br/>
</div>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>