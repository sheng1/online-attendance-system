<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>网上考勤系统</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />">
</head>

<div class="topdiv">

    <c:import url="/views/resources/banner.jsp"/></div>

<body>
<div class="choose">
    <a href="<c:url value="/staff/login" />">登录</a> |
    <a href="<c:url value="/staff/register" />">用户注册</a> |
    <a href="<c:url value="/manager/login" />">管理员登录</a><br/><br/>
    <p>登录有误，请重新登录！</p>
</div>
</body>


<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
